/**
* Biblioteca Auxiliar do framework Vue em https://cdn.jsdelivr.net/npm/vue/dist/vue.js
* @author Júlio César Lima Reis<juliolimareis@gmail.com>
* @description
*/

//Caso queira adicionar um link personalizado alterar Link_Vue com o novo link
//Guarda o link do framework Vue, true irá inciciar o link padão, abaixo:
// https://cdn.jsdelivr.net/npm/vue/dist/vue.js
var Link_Vue=true;
//Iniciando Vue; Se Link_Vue estiver null inicia link externo
InitVueScript();
//adicionando script personalizado
setScript('init-app.js');

function InitVueScript(){
  let link='https://cdn.jsdelivr.net/npm/vue/dist/vue.js';
  if(Link_Vue != true){
    link=Link_Vue;
  }
  let recaptchaScript = document.createElement('script');
  recaptchaScript.setAttribute('src', link);
  document.head.appendChild(recaptchaScript);
}

function setScript(script){
  setTimeout(function(){
    let recaptchaScript = document.createElement('script');
    recaptchaScript.setAttribute('src', script);
    document.head.appendChild(recaptchaScript);
  }, 100);
}

var app = new Vue({
el: '#app',
data:{
header:"Welcome to Vue",
pessoa:{
Nome:"Júlio",
Sobrenome:"César",
Idade:"27"
}
},
mounted(){

},
computed:{
exemple:{
get:function(){
return true;
},
set: function (paramet) {

}
}
},
methods:{
enableAlert:function(){
//
}
}
*/
