//axios fazer requisições
//'use strict'

Vue.component('test', {
  props: ['css'],
  template:'<div v-bind:class="this.css">Element Test</div>'
});

var send = new Vue({
  el:'#send',
  data:{
    nome:null,
    sobreNome:null,
    descricao:null,
    msg_alert:'',
    enable_alert:false,
    class_alert:null,
    enable_result_input:false,
    input:null
  },
  methods:{
    onSubmit:function(){
      if(!(this.nome && this.sobreNome && this.descricao)){
        this.class_alert = 'alert alert-warning';
        this.msg_alert   = 'Por favor Preencha o(s) campo(s)';
      }else {
        this.class_alert = 'alert alert-success';
        this.msg_alert   = 'Mensagem Enviada com sucesso!';
        this.sendJson();
      }
      this.enable_alert = true;
    },sendJson:function(){
      var data = {
        nome:      this.nome,
        sobreNome: this.sobreNome,
        descricao: this.descricao
      };
      // Send a POST request
      axios({
        method: 'post',
        url: 'hi',
        data: data
      }).then(function (response) {
        // handle success
        send.input=response.data;
        this.enable_result_input=true;
        console.log(response);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
        console.log('send...');
      });
      //end axios
    }
  }
});

var app = new Vue({
  el: '#app',
  data:{
    header:"Welcome to Vue",
    pessoa:{
      Nome:"Júlio",
      Sobrenome:"César",
      Idade:"27"
    }
  },
  mounted(){

  },
  computed:{
    exemple:{
      get:function(){
        return true;
      },
      set: function (paramet) {

      }
    }
  },
  methods:{
    enableAlert:function(){
      //
    }
  }
});
